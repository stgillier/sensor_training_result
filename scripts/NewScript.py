#!/usr/bin/env python

import rospy

from std_msgs.msg import Float64
from sensor_msgs.msg import JointState
var = 1
value_min = rospy.get_param("~value_min", -1)
value_max = rospy.get_param("~value_max", 1)

def callback(msg):
	global var
	if msg.name[0] == "laser_joint" :
		if (msg.position[0] > value_max):
			var = -1
		if (msg.position[0] < value_min):
			var = 1

rospy.init_node('Cmd_Vel_Node')
pub = rospy.Publisher('/laser_velocity_controller/command',Float64, queue_size=10)
rate = rospy.Rate(10)

rospy.Subscriber("/joint_states",JointState,callback, queue_size=10)


while not rospy.is_shutdown():
	msg = Float64()
	msg.data = var
	pub.publish(msg)
	rate.sleep()
