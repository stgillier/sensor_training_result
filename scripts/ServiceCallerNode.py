#!/usr/bin/env python

import roslib; roslib.load_manifest('laser_assembler')
import rospy; from laser_assembler.srv import *
from sensor_msgs.msg import PointCloud	

rospy.init_node("service_Caller_Node")
pub = rospy.Publisher('Cloud_Point',PointCloud, queue_size=10)
rate = rospy.Rate(0.2)

while not rospy.is_shutdown():
	rospy.wait_for_service("assemble_scans")

    	assemble_scans = rospy.ServiceProxy('assemble_scans', AssembleScans)
    	resp = assemble_scans(rospy.Time(0,0), rospy.get_rostime())
    	print "Got cloud with %u points" % len(resp.cloud.points)

	pub.publish(resp.cloud)
	rate.sleep()